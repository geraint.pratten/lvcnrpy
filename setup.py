#!/usr/bin/env python
#
# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup
from setuptools import find_packages

setup(name='lvcnrpy',
      version='3.0.0',
      description='Thin Python API to interact with the LVC NR catalogue',
      author='Edward Fauchon-Jones',
      author_email='edward.fauchon-jones@ligo.org',
      packages=find_packages(),
      url='https://git.ligo.org/edward.fauchon-jones/lvcnrpy',
      download_url='https://git.ligo.org/edward.fauchon-jones/lvcnrpy',
      install_requires=[
        'h5py',
        'numpy',
        'scipy',
        'matplotlib',
        'pytest',
        'colorama'],
      scripts=['bin/lvcnrcheck', 'bin/lvcnrjson'],
      license='GPLv3')
