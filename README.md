# lvcnrpy

`lvcnrpy` is a simple Python interface to the LVC-NR Waveform Repository.

## Installation

1.  **Install the package locally**

    The package can be installed directly from
    [git.ligo.org](https://git.ligo.org/) using the following snippet. Use your
    LIGO username `albert.einstein` and password when asked for
    authorisation

    ```bash
    $ pip install git+https://git.ligo.org/edward.fauchon-jones/lvcnrpy.git@v3.0.0#egg=lvcnrpy
    ```

2.  **Define LVC-NR Waveform Repository environment variable**

    Define the environment variable `LVCNR_DATADIR` as the path to your local
    clone of the LVC-NR Waveform Repository. If you are using a virtual
    environment this can be defined in your activation script. For a bash shell
    this could be done with

    ```bash
    export LVCNR_DATADIR=<path-to-lvc-nr-repo>
    ```

## Usage

### Checking HDF5 NR files against format specifications

A working draft of the possible formats HDF5 NR files may be committed in to the
LVC-NR Waveform Repository is avilable at
[Numerical Relativity Waveforms](https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/Waveforms/NR#BBH_Waveform_Catalog:_Metadata).
To check a HDF5 NR file against one of the format levels use the included
program `lvcnrcheck`. It is installed as a global program when this package is
installed.

-   **`lvcnrcheck` usage**

    ```bash
    lvcnrcheck [-h] [-c] [-f {1,2,3}] file

    Check hdf5 NR data meet LVCNR format specifications

    positional arguments:
      file                  name of file to check

    optional arguments:
      -h, --help            show this help message and exit
      -c, --col             use coloured output (default: False)
      -f {1,2,3}, --format {1,2,3}
                            format level to use (default: 1)
    ```

-   **`lvcnrcheck` example**

    For basic usage simple pass the file name to `lvcnrcheck`

    ```bash
    $ cd <path-to-lvc-nr-repo>
    $ lvcnrcheck ./Cardiff-UIB/GW150914-followup/q1.2_base_96.h5
    ```

    To check against a specific format level use the `-f` option

    ```bash
    $ cd <path-to-lvc-nr-repo>
    $ lvcnrcheck -f 2 ./Cardiff-UIB/GW150914-followup/q1.2_base_96.h5
    ```

    To add a coloured output use the `-c` flag

    To check against a specific format level use the `-f` option

    ```bash
    $ cd <path-to-lvc-nr-repo>
    $ lvcnrcheck -c ./Cardiff-UIB/GW150914-followup/q1.2_base_96.h5
    ```
