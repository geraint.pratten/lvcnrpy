# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .specs import *
from collections import OrderedDict


format1 = OrderedDict((
    ('General Fields', OrderedDict((
        ('type', Type),
        ('Format', Format),
        ('name', Name),
        ('alternative-names', AlternativeNames),
        ('NR-group', NRGroup),
        ('NR-code', NRCode),
        ('modification-date', ModificationDate),
        ('point-of-contact-email', PointOfContactEmail),
        ('INSPIRE-bibtex-keys', INSPIREBibtexKeys),
        ('license', License),
        ('Lmax', Lmax),
        ('simulation-type', SimulationType),
        ('auxiliary-info', AuxiliaryInfo),
        ('NR-techniques', NRTechniques)))),
    ('Error Assessment', OrderedDict((
        ('files-in-error-series', FilesInErrorSeries),
        ('comparable-simulation', ComparableSimulation),
        ('production-run', ProductionRun)))),
    ('CBC Parameters', OrderedDict((
        ("object1", Object1),
        ("object2", Object2),
        ("mass1", Mass1),
        ("mass2", Mass2),
        ("eta", Eta),
        ("f_lower_at_1MSUN", FLowerAt1MSUN),
        ("spin1x", Spin1x),
        ("spin1y", Spin1y),
        ("spin1z", Spin1z),
        ("spin2x", Spin2x),
        ("spin2y", Spin2y),
        ("spin2z", Spin2z),
        ("LNhatx", LNhatx),
        ("LNhaty", LNhaty),
        ("LNhatz", LNhatz),
        ("nhatx", Nhatx),
        ("nhaty", Nhaty),
        ("nhatz", Nhatz),
        ("Omega", Omega),
        ("eccentricity", Eccentricity),
        ("mean_anomaly", MeanAnomaly))))))

format1Interfield = OrderedDict((
    ("mass-ordering", MassOrdering),))

format2 = OrderedDict((
    ('mass1-vs-time', Mass1VsTime),
    ('mass2-vs-time', Mass2VsTime),
    ('spin1x-vs-time', Spin1xVsTime),
    ('spin1y-vs-time', Spin1yVsTime),
    ('spin1z-vs-time', Spin1zVsTime),
    ('spin2x-vs-time', Spin2xVsTime),
    ('spin2y-vs-time', Spin2yVsTime),
    ('spin2z-vs-time', Spin2zVsTime),
    ('position1x-vs-time', Position1xVsTime),
    ('position1y-vs-time', Position1yVsTime),
    ('position1z-vs-time', Position1zVsTime),
    ('position2x-vs-time', Position2xVsTime),
    ('position2y-vs-time', Position2yVsTime),
    ('position2z-vs-time', Position2zVsTime),
    ('LNhatx-vs-time', LNhatxVsTime),
    ('LNhaty-vs-time', LNhatyVsTime),
    ('LNhatz-vs-time', LNhatzVsTime),
    ('Omega-vs-time', OmegaVsTime)))

format2Interfield = OrderedDict((
    ('mass-vs-time-ordering', MassVsTimeOrdering),))

format3 = OrderedDict((
    ('remnant-mass-vs-time', RemnantMassVsTime),
    ('remnant-spinx-vs-time', RemnantSpinxVsTime),
    ('remnant-spiny-vs-time', RemnantSpinyVsTime),
    ('remnant-spinz-vs-time', RemnantSpinzVsTime),
    ('remnant-positionx-vs-time', RemnantPositionxVsTime),
    ('remnant-positiony-vs-time', RemnantPositionyVsTime),
    ('remnant-positionz-vs-time', RemnantPositionzVsTime)))
