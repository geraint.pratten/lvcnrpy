# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from subprocess import Popen, PIPE, STDOUT
from tempfile import NamedTemporaryFile
import os
from lvcnrpy.format.format import format1, format2, format3
from lvcnrpy.format.specs import Spec, GroupSpec, ROMSplineSpec, Phaselm, Amplm
import h5py as h5
from collections import OrderedDict
import numpy as np
from lvcnrpy.Sim import Sim


def lvcnrcheck(args, returncode=False):
    """Software API adapter for `lvcnrcheck`

    API to `lvcnrcheck` since `lvcnrcheck` only provides a CLI.

    Parameters
    ----------
    args: list
        List of command line argument to call `lvcnrcheck` with. Typically this
        should end with a path to a HDF5 file to validate against the LVCNR
        Wavform Reopsitory format specification.
    returncode: bool (optional)
        If `True`, return the return code of `lvcnrcheck`. Default is `False`.

    Returns
    -------
    output: str
        Standard output that would be recived from calling `lvcnrcheck` through
        the CLI interface with `h5Path`.

    (output, returncode): (str, bool)
        If `returncode` then return the standard output and the return code of
        `lvcnrcheck` as a tuple.
    """
    pipe = Popen(['lvcnrcheck']+args, stdout=PIPE, stderr=STDOUT)
    output = pipe.communicate()[0]
    if not returncode:
        return output
    else:
        return (output, pipe.returncode)


def createValidSim():
    """Create a temporary HDF5 file that is a valid LVC NR simulation.

    Returns
    -------
    file: NamedTemporaryFile
        `NamedTemporaryFile` object that is initialized as a HDF5 file that is
        a valid LVC NR simulation.
    """
    f = NamedTemporaryFile()
    sim = Sim(f.name, 'w')

    def createLeaves(nodes):
        for nodeKey, node in nodes.items():
            if isinstance(node, OrderedDict):
                createLeaves(node)
            elif isinstance(node(), Spec):
                node().createField(sim)

    createLeaves(format1)
    createLeaves(format2)
    createLeaves(format3)

    for l in range(2, 6):
        for m in range(-l, l+1):
            Phaselm(l, m).createField(sim)
            Amplm(l, m).createField(sim)

    sim.close()

    return f
